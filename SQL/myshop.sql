/* 1. Membuat Databases */
create database myshop;

/* 1.1 Memilih Nama Databases */
use myshop;

/* 2. Membuat Table Users */
CREATE TABLE users (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  name varchar(255),
  email varchar(255),
  password varchar(255)
);

/* 2.1 Membuat Table Categories */
CREATE TABLE categories (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  name varchar(255)
);

/* 2.2 Membuat Table Items */
CREATE TABLE items (
  id INTEGER AUTO INCREMENT PRIMARY KEY,
  name varchar(255),
  description varchar(255),
  price integer,
  stock integer,
  category_id integer,
  FOREIGN KEY (category_id) REFERENCES categories(id)
);

/* 3. Memasukkan Data ke Table Users */
INSERT INTO users (name, email, password) VALUES ('John Doe', 'john@doe.com', 'john123');
INSERT INTO users (name, email, password) VALUES ('Jane Doe', 'jane@doe.com', 'jenita123');

/* 4. Memasukkan Data ke Table Categories */
INSERT INTO categories (name) VALUES ('gadget');
INSERT INTO categories (name) VALUES ('cloth');
INSERT INTO categories (name) VALUES ('men');
INSERT INTO categories (name) VALUES ('women');
INSERT INTO categories (name) VALUES ('branded');

/* 5. Memasukkan Data ke Table Items */
INSERT INTO items (name, description, price, stock, category_id) VALUES ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1);
INSERT INTO items (name, description, price, stock, category_id) VALUES ('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2);
INSERT INTO items (name, description, price, stock, category_id) VALUES ('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);

/* 6. Menampilkan Data Users kecuali Password */
SELECT name, email
FROM users;

/* 7. Menampilkan Data items dengan Harga Lebih Besar atau Sama Dengan 1jt pada Table Users */
SELECT * FROM items WHERE price >= 1000000;

/* 8. Menampilkan Data items dengan Nama mipip Uniklo pada Table Users */
SELECT * FROM items WHERE name LIKE '%uniklo%';

/* 9. Menampilkan Data items dengan Harga Lebih Besar atau Sama Dengan 1jt pada Table Users */
SELECT items.name, items.description, items.price, items.stock, items.category_id,
    categories.name from items
    INNER JOIN categories on
    items.category_id = categories.id;

/* 10. Mengupdate Data Harga 2.5jt pada id=1 di Table Items */
UPDATE items SET price=25000000 where id=1;
