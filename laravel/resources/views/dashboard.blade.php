<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Welcome</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
  </head>
  <body>
    <h1>SELAMAT DATANG, {{ $first_name }} {{ $last_name }}</h1>
    <hr>
    <h3>Anda seorang : @if($gender === '1')
      Laki - Laki
      @elseif($gender === '2')
      Perempuan
      @else
      Other
      @endif
    </h3>
    <h3>Nationality : {{ $nationality }}</h3>
    <h3>Language Spoke : {{ $language }}</h3>
    <h3>Biodata : {{ $bio }}</h3>
    <hr>
    <h2>
      Terima Kasih telah bergabung di SanberBook. Social Media kita bersama!
    </h2><a href="/">Back To Home</a>
  </body>
</html>
