@extends('layouts.app')
@section('title')
Daftar Casts
@endsection

@push('scripts')
<script>
  $(function () {
    $("#castTable").DataTable();
  });
</script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.13.1/datatables.min.js"></script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.13.1/datatables.min.css"/>
@endpush

@section('content')
<div class="card card-outline card-secondary">
  <div class="card-header border-0">
    <h3 class="card-title"></h3>
    <div class="card-tools">
      <a href="/cast/create" class="btn btn-tool btn-sm">
      <i class="fas fa-plus"></i>
      </a>
    </div>
  </div>

    <table id="castTable" class="display">
      <thead>
          <tr>
              <th style="width: 20px">No</th>
              <th>Nama Cast</th>
              <th>Umur</th>
              <th>Biodata</th>
              <th>Action</th>
          </tr>
      </thead>
      <tbody>
        @forelse($cast as $key => $item)
        <tr>
          <td>{{ $key + 1 }}</td>
          <td>
            {{ $item->name }}
          </td>
          <td>
            {{ $item->umur }} Tahun
          </td>
          <td>
            {{ $item->bio }}
          </td>
          <td>
            <a href="/cast/{{$item->id}}" class="btn btn-info">
              <i class="fas fa-user"></i>
            </a>
            <a href="/cast/{{$item->id}}/edit" class="btn btn-secondary">
              <i class="fas fa-edit"></i>
            </a>

            <a href="cast/{{$item->id}}" class="btn btn-danger" onclick="
              var result = confirm('Are you sure you want to delete this record?');

              if(result){
                  event.preventDefault();
                  document.getElementById('delete-form-{{$item->id}}').submit();
              }">
              <i class="fas fa-trash"></i>
            </a>

          <form method="POST" id="delete-form-{{$item->id}}" action="{{route('cast.destroy', [$item->id])}}">
              {{csrf_field()}}
              <input type="hidden" name="_method" value="DELETE">
          </form>
          </td>
        </tr>

        @empty

        @endforelse
      </tbody>
    </table>
</div>
@endsection
