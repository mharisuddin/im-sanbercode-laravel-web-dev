@extends('layouts.app')
@section('title')
Profile Casts
@endsection

@section('content')
<div class="card card-primary">
  <div class="card-header">
  <h3 class="card-title"><a href="/cast">Back to Cast</a></h3>
  </div>

  <div class="card-body">
  <strong><i class="fas fa-book mr-1"></i> Name</strong>
  <p class="text-muted">
  {{ $cast->name }}
  </p>
  <hr>
  <strong><i class="fas fa-birthday-cake mr-1"></i> Umur</strong>
  <p class="text-muted">{{ $cast->umur }}</p>
  <hr>
  <strong><i class="fas fa-pencil-alt mr-1"></i> Biodata</strong>
  <p class="text-muted">
  {{ $cast->bio }}
  </p>
  <hr>
  </div>

  </div>
@endsection
