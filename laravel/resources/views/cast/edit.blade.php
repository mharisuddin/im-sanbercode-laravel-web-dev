@extends('layouts.app')
@section('title')
Edit Casts
@endsection

@section('content')
<div>
  <form action="/cast/{{ $cast->id }}" method="POST">
      @csrf
      @method('PUT')

      <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" name="name" id="name" value="{{ $cast->name }}" placeholder="Masukkan Nama Pemeran">
          @error('name')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>

      <div class="form-group">
          <label for="umur">Umur</label>
          <input type="number" class="form-control" name="umur" id="umur" value="{{ $cast->umur }}" placeholder="Masukkan Umur">
          @error('umur')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>

      <div class="form-group">
        <label for="bio">Biodata</label>
        <input type="text" class="form-control" name="bio" id="bio" value="{{ $cast->bio }}" placeholder="Tulis Biodata Pemeran">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

      <button type="submit" class="btn btn-primary">Update</button>
  </form>
</div>
@endsection
