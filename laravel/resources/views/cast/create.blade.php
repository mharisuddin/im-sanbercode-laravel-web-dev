@extends('layouts.app')
@section('title')
Tambah Casts
@endsection

@section('content')
<div>
  <form action="/cast" method="POST">
      @csrf

      <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama Pemeran">
          @error('name')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>

      <div class="form-group">
          <label for="umur">Umur</label>
          <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
          @error('umur')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>

      <div class="form-group">
        <label for="bio">Biodata</label>
        <textarea type="text" class="form-control" name="bio" id="bio" placeholder="Tulis Biodata Pemeran"></textarea>
        @error('name')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

      <button type="submit" class="btn btn-primary">Tambah</button>
  </form>
</div>
@endsection
