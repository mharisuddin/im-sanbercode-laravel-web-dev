<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Buat Account Baru</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
  </head>
  <body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/dashboard" method="POST">
      @csrf
      <label>First Name:</label><br /><br />
      <input type="text" name="first_name" placeholder="First Name" required />
      <br /><br />
      <label>Last Name:</label><br /><br />
      <input type="text" name="last_name" placeholder="Last Name" required />
      <br /><br />
      <label>Gender:</label>
      <br /><br />
      <input type="radio" name="gender" value="1"/>Male<br />
      <input type="radio" name="gender" value="2"/>Female<br />
      <input type="radio" name="gender" value="3"/>Other<br /><br />
      <label>Nationality:</label>
      <br /><br />
      <select name="nationality">
        <option value="indonesian">Indonesian</option>
        <option value="singapore">Singapore</option>
        <option value="malaysian">Malaysian</option>
      </select>
      <br /><br />
      <label>Language Spoken:</label>
      <br /><br />
      <input type="checkbox" name="language" value="Bahasa Indonesia"/> Bahasa Indonesia<br />
      <input type="checkbox" name="language" value="English"/> English<br />
      <input type="checkbox" name="language" value="Other"/> Other<br />
      <br /><br />
      <label>Bio:</label>
      <br /><br />
      <textarea name="bio" rows="10" cols="30"></textarea>
      <br />
      <button type="submit" value="store">Sign Up</button>
    </form>
  </body>
</html>
