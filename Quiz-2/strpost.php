<?php
  function ubah_huruf($string)
  {
    $output = "";

    for ($i = 0; $i < strlen($string); $i++) {
      $pos = strpos('abcdefghijklmnopqrstuvwxyz', $string[$i]);
      $output .= chr(ord('a') + $pos + 1);
    }
    return $output;
  }
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu
?>
